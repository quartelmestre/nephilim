# Nephilim for Foundry VTT

![GitLab Release](https://img.shields.io/gitlab/v/release/53340181?label=published%20release&color=0033cc)
![Dynamic JSON Badge](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fquartelmestre%2Fnephilim%2F-%2Fraw%2Fmain%2Fsystem.json&query=%24.version&label=current%20version&color=%23336600&prefix=v)
![Dynamic JSON Badge](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgitlab.com%2Fquartelmestre%2Fnephilim%2F-%2Fraw%2Fmain%2Fsystem.json&query=%24.compatibility.verified&prefix=v&label=Foundry%20verified)

This project is an implementation of [Nephilim v5](https://mnemos.com/livre/nephilim-legende-quintessence/) rules for [Foundry VTT](https://foundryvtt.com/).

There is a fairly advanced implementation for [Nephilim on Foundry](https://foundryvtt.com/packages/neph5e). This version intends to introduce amnesia rules for starting players, following ideas from [The Fugue System](https://jameswallis.itch.io/the-fugue-system) by Jamed Wallis.

## Installation

Download the [latest release](https://gitlab.com/quartelmestre/nephilim/-/releases) zip asset and unzip it as a folder under Foundry's `Data/systems/` folder.
